import os
import sys
import requests
import webbrowser
import json
import bs4
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import shutil
from PIL import ImageOps, Image
from threading import Thread
from time import sleep

SCOPES = ['read:statuses', 'read:accounts', 'write:accounts']

def read_token_file(token_file='.token'):
    if not os.path.exists(token_file):
        print('No token file found. See details on usage.')
        sys.exit(1)
    token_file = open(token_file, 'r')
    token_file_lines = token_file.readlines()
    server_url = token_file_lines[0].strip()
    token = token_file_lines[1].strip()
    return server_url, token

def authorize(client_id, client_secret, server_url):
    print('Running authorization stage.')
    print('No authorization file found. Opening oauth browser tab.')
    authorization_url = f'https://{server_url}/oauth/authorize?client_id={client_id}&scope={"+".join(SCOPES)}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code'
    webbrowser.open(authorization_url)
    print('If not open please navigate manually to this link:')
    print(authorization_url)
    print('Then copy the code from the browser and paste it here:')
    code = input('Code: ')
    authorization_code = code.strip()
    token_url = f'https://{server_url}/oauth/token'
    token_request = requests.post(token_url, data={
        'client_id': client_id,
        'client_secret': client_secret,
        'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
        'grant_type': 'authorization_code',
        'code': authorization_code,
        'scope': ' '.join(SCOPES)
        })
    if token_request.status_code != 200:
        print('Error getting token:')
        print(token_request.text)
        sys.exit(1)
    token = json.loads(token_request.text)['access_token']
    open('.token', 'w').write(f'{server_url}\n{token}')
    print('URL and Token saved to .token file.')

def check_account(headers, server_url):
    test_url = f'https://{server_url}/api/v1/accounts/verify_credentials'
    test_request = requests.get(test_url, headers=headers)
    if test_request.status_code != 200:
        print('Something went wrong with auth.')
        print(test_request.text)
        sys.exit(1)
    print('Token is valid.')
    user_name = json.loads(test_request.text)['username']
    user_id = json.loads(test_request.text)['id']
    display_name = json.loads(test_request.text)['display_name']
    print(f'Logged in as {user_name} [{display_name}] ({user_id}).')
    avatar_url = json.loads(test_request.text)['avatar_static']
    print(f'Avatar url: {avatar_url}')
    return user_id, avatar_url, user_name, display_name

def check_currently_calm(display_name, positive_suffix, negative_suffix):
    if display_name.endswith(positive_suffix):
        return True
    if display_name.endswith(negative_suffix):
        return False
    return None


def download_and_invert_avatar(url, headers, isCalmNamed):
    avatar = requests.get(url, headers, stream=True)
    basename = url.split('/')[-1]
    avatar_ext = basename.split('.')[-1]
    avatar_path = f'avatar.{avatar_ext}' if isCalmNamed is True or isCalmNamed is None else f'avatar_inverted.{avatar_ext}'
    inverted_avatar_path = f'avatar_inverted.{avatar_ext}' if isCalmNamed is True or isCalmNamed is None else f'avatar.{avatar_ext}'
    if avatar.status_code != 200:
        print('Something went wrong with avatar.')
        print(avatar.text)
        sys.exit(1)
    with open(avatar_path, 'wb') as f:
        avatar.raw.decode_content = True
        shutil.copyfileobj(avatar.raw, f)
    print(f'Avatar saved to {avatar_path}.')
    image = Image.open(avatar_path)
    if image.mode == 'RGBA':
        r,g,b,a = image.split()
        rgb_image = Image.merge('RGB', (r,g,b))
        inverted_image = ImageOps.invert(rgb_image)
        r2,g2,b2 = inverted_image.split()
        final_transparent_image = Image.merge('RGBA', (r2,g2,b2,a))
        final_transparent_image.save(inverted_avatar_path)
        print(f'Inverted avatar saved to {inverted_avatar_path}.')
    else:
        inverted_image = ImageOps.invert(image)
        inverted_image.save(inverted_avatar_path)
        print(f'Inverted avatar saved to {inverted_avatar_path}.')
    return avatar_ext

def check_last_sentiment(headers, server_url, user_id, margin, limit):
    toots_url = f'https://{server_url}/api/v1/accounts/{user_id}/statuses?limit={limit}'
    toots = requests.get(toots_url, headers=headers)
    if toots.status_code != 200:
        print('Something went wrong with toots.')
        print(toots.text)
        sys.exit(1)
    toots = json.loads(toots.text)
    sentiments = []
    for toot in toots:
        toot_content = toot['reblog']['content'] if toot['content'] == '' else toot['content']
        toot_text = bs4.BeautifulSoup(toot_content, 'html.parser').get_text(separator=' ').strip()
        print(f'[{toot["created_at"]}] {toot_text}')
        analyzer = SentimentIntensityAnalyzer()
        sentiment = analyzer.polarity_scores(toot_text)
        print(f'Sentiment: {sentiment["compound"]}')
        sentiments.append(sentiment["compound"])
    sentiments = sum(sentiments) / len(sentiments)
    print(f'Average sentiment: {sentiments}')
    if (sentiments > margin):
        print('Calm.')
        return True
    if (sentiments < -margin):
        print('Not calm.')
        return False
    print('Neutral assume calm.')
    return True # assume calm

def update_profile(headers, server_url, user_id, display_name, avatar_ext, positive_suffix, negative_suffix, isCalmNamed, isCalmStatus):
    upate_url = f'https://{server_url}/api/v1/accounts/update_credentials'
    update_data = {}
    update_files = {}
    if isCalmNamed is None and isCalmStatus is True:
        # we force update display name because it does not contain the fix
        update_data['display_name'] = f'{display_name} {positive_suffix}'
        # we assume the avatar is already in calm state here
    if isCalmNamed is None and isCalmStatus is False:
        update_data['display_name'] = f'{display_name} {negative_suffix}'
        update_files['avatar'] = open(f'avatar_inverted.{avatar_ext}', 'rb')
    # if isCalmNamed and isCalmStatus is True:
        # we dont want to do anything
    if isCalmNamed is True and isCalmStatus is False:
        update_data['display_name'] = f'{display_name} {negative_suffix}'
        update_files['avatar'] = open(f'avatar_inverted.{avatar_ext}', 'rb')
    if isCalmNamed is False and isCalmStatus is True:
        update_data['display_name'] = f'{display_name} {positive_suffix}'
        update_files['avatar'] = open(f'avatar.{avatar_ext}', 'rb')
    # if isCalmNamed is False and isCalmStatus is False:
        # we dont do anything
    # check that at least one field is updated
    if len(update_data) > 0 or len(update_files) > 0:
        response = requests.patch(upate_url, headers = headers, data = update_data, files = update_files)
        if response.status_code != 200:
            print('Something went wrong with update.')
            print(response.text)
            sys.exit(1)
        print('Updated profile.')
    print('Nothing to do.')

def routine(headers, server_url, user_id, margin, display_name, avatar_ext, positive_suffix, negative_suffix, isCalmNamed, limit):
    isCalmStatus = check_last_sentiment(headers, server_url, user_id, margin, limit)
    update_profile(headers, server_url, user_id, display_name, avatar_ext, positive_suffix, negative_suffix, isCalmNamed, isCalmStatus)

def call_at_interval(period, callback, args):
    while True:
        callback(*args)
        sleep(period)
def setInterval(period, callback, *args):
    Thread(target=call_at_interval, args=(period, callback, args)).start()

def main():
    if os.environ.get('CLIENT_ID') is not None and os.environ.get('CLIENT_SECRET') is not None and os.environ.get('SERVER_URL') is not None:
        authorize(client_id=os.environ['CLIENT_ID'], client_secret=os.environ['CLIENT_SECRET'], server_url=os.environ['SERVER_URL'])

    positive_suffix = '(calm)'
    if os.environ.get('POSITIVE_SUFFIX') is not None:
        positive_suffix = os.environ['POSITIVE_SUFFIX']

    negative_suffix = '(not calm)'
    if os.environ.get('NEGATIVE_SUFFIX') is not None:
        negative_suffix = os.environ['NEGATIVE_SUFFIX']

    margin = 0.2
    if os.environ.get('MARGIN') is not None:
        margin = float(os.environ['MARGIN'])

    interval = (5 * 60) / 200 # you are allowed 300 requests every 5 minutes so this is "safe" and "fast" setting
    if os.environ.get('INTERVAL') is not None:
        interval = float(os.environ['INTERVAL'])

    limit = 3 # default check against 3 latest toots
    if os.environ.get('LIMIT') is not None:
        limit = int(os.environ['LIMIT'])

    if os.environ.get('ACCESS_TOKEN') is None:
        server_url, token = read_token_file()
    else:
        server_url = os.environ['SERVER_URL']
        token = os.environ['ACCESS_TOKEN']


    headers = {'Authorization': f'Bearer {token}'}

    user_id, avatar_url, user_name, display_name = check_account(headers, server_url)

    isCalmNamed = check_currently_calm(display_name, positive_suffix, negative_suffix)

    avatar_ext = download_and_invert_avatar(avatar_url, headers, isCalmNamed)

    if isCalmNamed:
        display_name = display_name.replace(positive_suffix, '')
    if isCalmNamed is False:
        display_name = display_name.replace(negative_suffix, '')
    display_name = display_name.strip()

    setInterval(interval, routine, headers, server_url, user_id, margin, display_name, avatar_ext, positive_suffix, negative_suffix, isCalmNamed, limit)

    print('Done.')


main()
