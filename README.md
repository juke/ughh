# [ughh](https://code.juke.fr/kay/ughh)
set a specific name suffix and invert your profile picture colors depending on if your posts are calm or not

## warning
this is a quick prototype/script thing i wrote for myself in a couple of hours

so its not ready for any kinds of actual production/serious usage and whatnot

**(dont use for now if you dont understand what is going on in there)**

## what its it
1. this is a little program that runs on a computer and that will fetch your latest toot at a set interval
2. it will then run sentiment analysis on it and depending on how negative or positive it is will try to figure out
if you are currently calm or not calm (within a configurable margin of error that defaults to -0.2 to 0.2 that you can configure
in which case it just assumes you are calm)
3. then depending on what the sentiment is
   1. if you are calm your name get updated with whatever suffix you set it to (defaults to `(calm)`)
   2. if you are not calm name gets updated with whatever suffix you set it to (defaults to `(not calm)`)
and your profile picture is set to inverted colors

so to put it in other words
- when you make a post on mastodon 
   - it will **invert your avatar** colors and set a customizable word after your name if it is **negative**
   - set back to **default avatar** colors and set a customizable word after your name if it is positive**

## configuration
there are several environment variables that can be passed to override the defaults

some are required on setup (see setup below) before being able to use the service

```bash
# setup variables, only used to generate token when needed (on first run usually)
CLIENT_ID=abc... # see setup part of readme
CLIENT_SECRET=abc... # see setup of readme
SERVER_URL=domain.tld # do not pass https or a trailing slash

ACCESS_TOKEN=abc... # can be used in conjuction with SERVER_URL to bypass .token file (see setup for info on .token file)

INTERVAL=(60*5)/200 # how many seconds to wait after each time we check the latest toot
# the default is low to make it "snappy" but if you have a slow connection maybe not the best idea
# the maximum allowed is 300 requests per 5 minutes so here 200 was used in the formula for a bit of margin

MARGIN=0.2 # how much is considered the "margin of error" for the sensitivity of the sentiment detection
# -1 is negative maximum and +1 is positive maximum so with 0.2 anything that is between -0.2 and 0.2 is considered "calm"

LIMIT=3 # the number of latest toots that are used to average your current sentiment
# just gets the sentiment for 3 latest toots then divides it by 3 for the average

NEGATIVE_SUFFIX="(not calm)" # what text will be added after your display name when posting negatively

POSITIVE_SUFFIX="(calm)" # what text will be added after your display name when posting positively
```

so a couple example commands would look like
```bash
# for setup phase and forcing new auth flow
$ CLIENT_ID=abc... CLIENT_SECRET=abc... SERVER_URL=subdomain.domain.tld python -m ughh.main

# regular usage with defaults
$ python -m ughh.main

# with custom settings
$ INTERVAL=10 MARGIN=0.4 LIMIT=1 NEGATIVE_SUFFIX=">:{" POSITIVE_SUFFIX=":}" python -m ughh.main

# you can directly pass ACCESS_TOKEN and SERVER_URL to bypass the need for a .token file
$ ACCESS_TOKEN=abc... SERVER_URL=subdomain.domain.tld python -m ughh.main
```

## setup

### mastodon application
the first thing you want to do is create a mastodon application i'm not sure if these are

admin specific or if anybody can create them

it is in your profile settings then development

the only permissions it requires are:
- read:accounts
- read:statuses
- write:accounts

this is to be able to read the toots and update the display name and avatar

you will need to write down the **client key** and **client secret** if you want to do auto setup

(you can also grab the **access token** if you want to just manually create the config file)

### installing
this app runs on python so make sure you have that installed

then ideally i will publish a package to pip but for now you have to clone everything locally so

```bash
$ git clone https://code.juke.fr/kay/ughh.git

$ cd ughh
```
(*then ideally you create a python virtual environment to not have dependency conflicts and whatnot but this is out of scope for now in this readme*)
then we install the python dependencies

```bash
$ python -m pip install -r requirements.txt
```

and now you shoud be good to go onto the next step of authentication

### oauth guided setup
if you just run the service with the environment variables `CLIENT_ID`, `CLIENT_SECRET` and `SERVER_URL`
(again make sure no https in the url and no trailing slash) it will automatically open an oauth page
to ask you to login and copy a callback code it gives you to paste into the console

it will then use that code to fetch the token we need and save it to `.token` file along with the server url
(**so yeah make sure that file stays safe lol**)

```bash
$ CLIENT_ID=abc... CLIENT_SECRET=abc... SERVER_URL=subdomain.domain.tld python -m ughh.main
```

### manual setup
or you can just manually create a `.token` file with the token given on creation of the application
the format of the file is simpy on firstline the server_url and on second line the token like so
```
subdomain.domain.tld
abc...
```

## usage

once you are done with the setup stage you can simply run the command without any environment variables to use the defaults
mentioned previously or use your own values, again refer to the configuration part of the readme

```bash
# for setup phase and forcing new auth flow
$ CLIENT_ID=abc... CLIENT_SECRET=abc... SERVER_URL=subdomain.domain.tld python -m ughh.main

# regular usage with defaults
$ python -m ughh.main

# with custom settings
$ INTERVAL=10 MARGIN=0.4 LIMIT=1 NEGATIVE_SUFFIX=">:{" POSITIVE_SUFFIX=":}" python -m ughh.main

# you can directly pass ACCESS_TOKEN and SERVER_URL to bypass the need for a .token file
$ ACCESS_TOKEN=abc... SERVER_URL=subdomain.domain.tld python -m ughh.main
```

*lawsuit liability disclaimer: there is a slight possibility of crunching overtime of lossy image format avatars if restarting the service often*


## Development

To clone the repository locally:

```bash
$ git clone https://code.juke.fr/kay/ughh.git
```

## Contributing

Feel free to contact via the email bellow with a patch or anything.

### Issues
Open new issues by mailing [issues+ughh@juke.fr](mailto:issues+ughh@juke.fr)

---
beep boop

